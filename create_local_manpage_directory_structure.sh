#!/usr/bin/env bash

function has_feature() {

    if ! [ -z ${!1+set} ]; then

        return 0
    else

        return 1
    fi
}


function error() {

   echo "$1" >&2
   exit 1
}


if ! has_feature "ASHM_UTILS_DIRECTORIES"; then

    error "directory utilities not loaded"
fi

if ! has_feature "ASHM_UTILS_BASHRC"; then

    error "bashrc utilities not loaded"
fi


# Define path, export so that it is avaialbe for other scripts without needing to source bashrc
export ASHM_LOCAL_MANPAGE_HOME="${HOME}/.local/man"

create_directory_if_not_exist "$ASHM_LOCAL_MANPAGE_HOME"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man1"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man2"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man3"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man4"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man5"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man6"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man7"
create_directory_if_not_exist "${ASHM_LOCAL_MANPAGE_HOME}/man8"

add_section_to_bashrc "Manpage"
add_environment_variable_in_bashrc_section "ASHM_LOCAL_MANPAGE_HOME" "$ASHM_LOCAL_MANPAGE_HOME" "Manpage"
add_environment_variable_in_bashrc_section "MANPATH" "\$(manpath -g):\$ASHM_LOCAL_MANPAGE_HOME" "Manpage"

