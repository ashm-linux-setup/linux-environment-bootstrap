#!/usr/bin/env bash
#
# Setup a linux environment, this is not intended to be
# idempotent things like extending the PATH var or 
# adding sections are going to result in duplication
# if the script is run twice.
#
# The indiviudal components of the bootstrap process have
# been separated into their own scripts to make it reasonably
# simple to run them independently

# setup pre-requsites
source ./setup_prerequsites.sh

# configure system
source ./create_xdg_directory_structure.sh
source ./create_remotes_directory_structure.sh
source ./create_home_directory_structure.sh
source ./create_local_manpage_directory_structure.sh
source ./install_bash_utils.sh
#source ./install_remotes.sh
exit
