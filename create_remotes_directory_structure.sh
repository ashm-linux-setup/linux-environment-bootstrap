#!/usr/bin/env bash

function has_feature() {

    if ! [ -z ${!1+set} ]; then

        return 0
    else

        return 1
    fi
}

function error() {

   echo "$1" >&2
   exit 1
}



if ! has_feature "ASHM_UTILS_DIRECTORIES"; then

    error "directory utilities not loaded"
fi

if ! has_feature "ASHM_UTILS_BASHRC"; then

    error "bashrc utilities not loaded"
fi


function setup_directory() {

    create_directory_if_not_exist "$1"

    if ! [ -z "$2" ]; then

	add_environment_variable_in_bashrc_section "$2" "$1" "Remotes"
    fi
}


# Setup remote directory (where remote repositories are downloaded to)
add_section_to_bashrc "Remotes"
setup_directory "$HOME/.remotes" "ASHM_REMOTES_HOME" 


# helper for other scripts that may occurr in other sessions
export ASHM_REMOTES_HOME="$HOME/.remotes"

