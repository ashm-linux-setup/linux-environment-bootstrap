#!/usr/bin/env bash


if [ -z $ASHM_REMOTES_HOME ]; then
    echo "ASHM_REMOTES_HOME not setup up please run the bootstrap script"
    exit
fi

bash_utils_repo="git@gitlab.com:ashm-linux-setup/bash-utils.git"

get_or_update_remote "$bash_utils_repo"
install_remote "$bash_utils_repo"
