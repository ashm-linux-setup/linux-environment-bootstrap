#!/usr/bin/env bash
#
# Creates the standard XDG directories if they do not
# exist and add environment variable exports to
# .bashrc

function has_feature() {

    if ! [ -z ${!1+set} ]; then

        return 0
    else

        return 1
    fi
}

function error() {

   echo "$1" >&2
   exit 1
}



if ! has_feature "ASHM_UTILS_DIRECTORIES"; then

    error "directory utilities not loaded"
fi

if ! has_feature "ASHM_UTILS_BASHRC"; then

    error "bashrc utilities not loaded"
fi


function setup_directory() {

    create_directory_if_not_exist "$1"
    add_environment_variable_in_bashrc_section "$2" "$1" "XDG"
}


# Setup XDG home directories
add_section_to_bashrc "XDG"
setup_directory "$HOME/.local/data" "XDG_DATA_HOME"  
setup_directory "$HOME/.local/state" "XDG_STATE_HOME" 
setup_directory "$HOME/.local/bin" "XDG_BIN_HOME"   
include_environment_variable_in_path_in_bashrc_section "XDG_BIN_HOME" "XDG"


export XDG_DATA_HOME="$HOME/.local/data"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_BIN_HOME="$HOME/.local/bin"
export PATH="$XDG_BIN_HOME:$PATH"

