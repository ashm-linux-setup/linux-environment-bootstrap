#!/usr/bin/env bash


source "./setup_prerequsites.sh"

if [ -z $ASHM_REMOTES_HOME ]; then
    echo "ASHM_REMOTES_HOME not setup up please run the bootstrap script"
    exit
fi


# install repositories
cached_dir=$(pwd)

while IFS= read -r remote_repo_url; do

    if  echo "$remote_repo_url" | grep -qE "^#"; then

	echo "skip: $remote_repo_url"
	continue
    fi

    get_or_update_remote "$remote_repo_url"
    install_remote "$remote_repo_url"

    continue


    cd "$ASHM_REMOTES_HOME"

    # get the install directory name
    util_dir_name=$(basename "$util_repo_url" .git)
    echo "$util_dir_name"

    # get or update
    if [ ! -d "${ASHM_REMOTES_HOME}/${util_dir_name}" ]; then

        git clone "$util_repo_url"
    else

        cd "${ASHM_REMOTES_HOME}/${util_dir_name}"
        git pull
        cd ..
    fi

    # run the setup as it should updates as well as setup
    if [ -e "${ASHM_REMOTES_HOME}/${util_dir_name}/setup.sh" ]; then

       "${ASHM_REMOTES_HOME}/${util_dir_name}/setup.sh"
    fi

    cd "${cached_dir}"
done < "./remote_repos"

cd "$cached_dir"
