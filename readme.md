# linux workspace bootstrap

Bootstrapper that setup standard scripts and config on a newly installed linux system.

## Getting started

1. Run __bootstrap.sh__ this will:

- Setup the XDG directories and environment variables
- Create the appropriate directories for the remote scripts to be downloaded into
- Create the default home directory structure

2. Source .bashrc

- n. This is so that the paths/feature flags are setup that may be needed by remote installs

2. Run __install-remotes.sh__ this will:

- Downloads and run the setup script for each remote
- Dowload into ASHM_REMOTES_HOME 
- Run setup.sh in the remote if there is one
- Remotes are git repos in remote_repos


## Extras

- generate-ssh-key.sh  ,helper for generating an ssh key (saves looking up the command)

