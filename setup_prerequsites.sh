#!/usr/bin/env bash
#
# Load the required utilties and create the local directory if
# if does not exist.
#
# Need to source the utilities manually as the bootstrap process
# is setting up the environment so have to assume they have not
# been setup to always be loaded on the system that they are 
# being run on.

local_directory_path="./local/"

function get_and_source_utils() {

    if [ -z $1 ]; then

        echo "get_and_source_utils: no file added" >&2
	exit 1
    fi

    local util_file_path="${local_directory_path}${1}"
  
    curl https://gitlab.com/ashm-linux-setup/bash-utils/-/raw/main/$1 \
	    -o $util_file_path \
	    -s

    source $util_file_path > /dev/null
}


# setup pre-requsites
mkdir -p $local_directory_path

get_and_source_utils "bashrc_utils.sh"
get_and_source_utils "directory_utils.sh"
get_and_source_utils "remotes_utils.sh"
