#!/usr/bin/env bash

while getopts "a:f:u:" opt; do
	case $opt in
		a) algorithm="$OPTARG"
		;;
		f) file="$OPTARG"
		;;
		u) username="$OPTARG"
		;;
	esac
done

algorithm=${algorithm:="ed25519"}
file=${file:="$HOME/.ssh/id_${algorithm}"}
username=${username:="williampaulmoore@gmail.com"}

echo "a: $algorithm"
echo "f: $file"
echo "u: $username"


# list supported algorithms
ssh -Q key

# generate key
ssh-keygen -t "$algorithm" -C "$username" -f "$file"

if [ $XDG_SESSION_TYPE == "wayland" ]; then

	cat "${file}.pub" | wl-copy # only works on wayl
fi
