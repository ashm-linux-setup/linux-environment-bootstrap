#!/usr/bin/env bash

function has_feature() {

    if ! [ -z ${!1+set} ]; then

        return 0
    else

        return 1
    fi
}

function error() {

   echo "$1" >&2
   exit 1
}


if [ -z "$ASHM_REMOTES_HOME" ] || ! [ -d "$ASHM_REMOTES_HOME" ]; then

    error "Remotes directory has not been set up"
fi

if [ -z "$XDG_BIN_HOME" ] || ! [ -d "$XDG_BIN_HOME" ]; then

    error "XDG bin directory has not been set up"
fi

if ! has_feature "ASHM_UTILS_DIRECTORIES"; then

    error "directory utilities not loaded"
fi

if ! has_feature "ASHM_UTILS_BASHRC"; then

    error "bashrc utilities not loaded"
fi


# Define file paths

alias_filename="alias_home_directories.sh"
remotes_directory_path="${ASHM_REMOTES_HOME}/home-directory-structure"
remotes_alias_file_path="${remotes_directory_path}/${alias_filename}"


function setup_directory() {

    create_directory_if_not_exist "$1"
 
    sed -i "/alias $2/d" "$remotes_alias_file_path"
    echo "alias $2=\"cd $1\"" >> "$remotes_alias_file_path"
}


# Create home directory aliases files
if ! [ -d $remotes_directory_path ]; then

    mkdir -p "$remotes_directory_path"
fi

if ! [ -e "$remotes_alias_file_path" ]; then

    echo "#!/use/bin/env bash" > "$remotes_alias_file_path"
    echo "" >> "$remotes_alias_file_path"
    chmod +x "$remotes_alias_file_path"
fi 


# create home directories
setup_directory "$HOME/m"   "m"
setup_directory "$HOME/m/b" "b"
setup_directory "$HOME/m/k" "k"
setup_directory "$HOME/m/p" "p"
setup_directory "$HOME/m/s" "s"
setup_directory "$HOME/m/q" "q"


# link alias file to local bin directory which should be in the path
if [ -e "${XDG_BIN_HOME}/${alias_filename}" ]; then

    rm "${XDG_BIN_HOME}/${alias_filename}"
fi
ln -s "$remotes_alias_file_path" "${XDG_BIN_HOME}/${alias_filename}"

# source alias in .bashrc
add_section_to_bashrc "Home directories"
source_file_in_bashrc_section "$alias_filename" "Home directories"

